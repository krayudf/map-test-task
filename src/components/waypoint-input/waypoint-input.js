import React, {Component} from "react";

import "./waypoint-input.css";

export default class Waypointinput extends Component{

  onWaypointInputEnter = (event) => {
    if(event.keyCode === 13){
      this.props.onAddNewWaypoint(event.target.value);
    }
  }

  render(){
    return (
        <input className="search-input" id="searchInput" onKeyUp={this.onWaypointInputEnter}  type="text"/>
    );
  }
}
