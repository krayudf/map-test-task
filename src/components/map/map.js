import React, {Component} from "react";

import "./map.css";

export default class Map extends Component{

  constructor(props){
    super(props);
    this.map = null;
    this.waypointsCoordinates = [];
  }

  componentDidMount(){
    window.addEventListener('load', () => {
        window.ymaps.ready(() => {
          this.map = new window.ymaps.Map('map', {
            center: [55.76, 37.64],
            zoom: 9,
            controls: ["zoomControl"]
          });
        });
      });
  }

  drawPath = () => {
    const polylineVertices = this.props.waypoints.map((waypoint) => waypoint.coords);
    let polyline = new window.ymaps.Polyline(polylineVertices, {}, {
            strokeColor: "#3caa3c",
            strokeWidth: 4,
            strokeOpacity: 0.7
        });

    this.map.geoObjects.removeAll();
    this.map.geoObjects.add(polyline);
  }

  drawWaypoints = () => {
    this.props.waypoints.forEach((waypoint) => {
      let pointOnMap = new window.ymaps.Placemark(waypoint.coords, {
          balloonContent: waypoint.name
      }, {
          preset: 'islands#circleIcon',
          iconColor: '#3caa3c',
          draggable: true
      });

      pointOnMap.events.add(['dragend'], (e) => {
        let newCoords = e.get("target").geometry.getCoordinates();

        window.ymaps.geocode(newCoords).then((res) => {
            var firstGeoObject = res.geoObjects.get(0);
            let newName = firstGeoObject.getAddressLine();
            let newBounds = firstGeoObject.properties.get('boundedBy');
            this.props.onWaypointMoved(waypoint.id, newName, newCoords, newBounds);
        });


    });


      this.map.geoObjects.add(pointOnMap);
    });
  }

  focusOnLastWaypoint = () => {
    this.map.setBounds(this.props.bounds, {checkZoomRange: true});
  }

  render(){
    if(this.map){
      this.drawPath();
      this.drawWaypoints();
      this.focusOnLastWaypoint();
    }

    return(
      <div id="map" className="map"></div>
    )
  }
}
