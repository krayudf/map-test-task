import React, {Component} from "react";

import WaypointListItem from "../waypoint-list-item/waypoint-list-item";

import "./waypoints-list.css";

export default class WaypointsList extends Component{

  componentDidMount (){
      window.$(() => {
        window.$("#sortable").sortable();
        window.$("#sortable").disableSelection();
        window.$("#sortable").sortable({"update": () => {
          let elements = document.getElementsByClassName("waypoint-list-item");
          let waypointsIdSequence = [];
          for(let i = 0; i < elements.length; i++){
            waypointsIdSequence.push(parseInt(elements[i].getAttribute("waypointid")));
          }
          this.props.onWaypointsSequenceChange(waypointsIdSequence);
        }});
      } );
  }

    render(){
      return (
          <ul id="sortable" className="waypoint-list">
            {this.props.waypoints.map(({id, name}) => <WaypointListItem onWaypointRemove={this.props.onWaypointRemove} key={id} name={name} id={id}/>)}
          </ul>
      );
    }
}
