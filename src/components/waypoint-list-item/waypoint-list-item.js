import React, {Component} from "react";

import "./waypoint-list-item.css";

export default class WaypointListItem extends Component{

  onRemoveBtnClick = () => {
    this.props.onWaypointRemove(this.props.id);
  }

  render(){
    return (
      <li className="waypoint-list-item" waypointid={this.props.id}>
        <div className="waypoint-title">{this.props.name}</div>
        <div className="remove-item-section"><button className="remove-item-btn" onClick={this.onRemoveBtnClick}><i className="fas fa-trash-alt"></i></button></div>
      </li>
    );
  }
}
