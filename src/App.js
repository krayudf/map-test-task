import React, { Component } from 'react';


import Map from "./components/map/map";
import WaypointsList from "./components/waypoints-list/waypoints-list";
import Waypointinput from "./components/waypoint-input/waypoint-input"

import './App.css';

export default class App extends Component {

  constructor(){
    super();
    this.idCounter = 0;

    this.state = {
        waypoints: [],
        bounds:[]
    };
  }

  onEnterNewWaypoint = (waypointName) => {
    window.ymaps.geocode(waypointName, {results: 1}).then((res) => {
      let firstGeoObject = res.geoObjects.get(0);
      if(res.geoObjects.get(0))
      {
          const pointCoords = firstGeoObject.geometry.getCoordinates();
          this.setState((prevState) => {
            let newWaypointsArr = [...prevState.waypoints];
            let bounds = firstGeoObject.properties.get('boundedBy');
            newWaypointsArr.push({id:this.idCounter++, name:waypointName, coords:pointCoords, bounds});
            return {
              waypoints:newWaypointsArr,
              bounds
            };
          });
      }
      else {
        alert("Такой точки не найдено");
      }
    });
  }

  onWaypointMoved = (waypointId, newName, newCoords, newBounds) => {
    this.setState((prevState) => {
      let newWaypoints = [...prevState.waypoints];
      newWaypoints.forEach((item, index) => {
        if(item.id === waypointId){
          newWaypoints[index].name = newName;
          newWaypoints[index].coords = newCoords;
        }
      });

      return {
        waypoints:newWaypoints,
        bounds:newBounds
      }
    })
  }

  onWaypointRemove = (id) => {
    this.setState((prevState) => {
        let removeElementIndex = undefined;
        prevState.waypoints.forEach((item, index) => {
          if(item.id === id){
            removeElementIndex = index;
          }
        });

      let newWaypiontsArray = [...prevState.waypoints.slice(0, removeElementIndex), ...prevState.waypoints.slice(removeElementIndex + 1)];

      let waypointsAmount = newWaypiontsArray.length;
      let bounds = (waypointsAmount >= 1) ? newWaypiontsArray[waypointsAmount - 1].bounds : prevState.bounds;

      return {
        waypoints:newWaypiontsArray,
        bounds
      }
    });


  }

  onWaypointsSequenceChange = (waypointsIdSequence) => {
    this.setState((prevState) => {
      let newWaypointsSequence = [];
      prevState.waypoints.forEach(waypoint => {
        let waypointId = waypoint.id;
        let newIndexInArray = waypointsIdSequence.indexOf(waypointId);
        newWaypointsSequence[newIndexInArray] = waypoint;
      });

      return {
        waypoints:newWaypointsSequence
      }
    });
  }

  render() {
    return (
      <div className="app">
        <div>
          <div className="search">
            <Waypointinput onAddNewWaypoint={this.onEnterNewWaypoint}/>
          </div>
          <div className="list">
            <WaypointsList onWaypointsSequenceChange={this.onWaypointsSequenceChange} onWaypointRemove={this.onWaypointRemove} waypoints={this.state.waypoints}/>
          </div>
        </div>
        <Map onWaypointMoved={this.onWaypointMoved} waypoints={this.state.waypoints} bounds={this.state.bounds}/>
      </div>
    );
  }
}
